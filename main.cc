/**
@file main.cc

Plik z funkcja main() do cwiczenia 4 na laboratoriach z AISDI.

@author
Pawel Cichocki, Michal Nowacki

@date
last revision
- 2005.12.01 Michal Nowacki: lab #4
- 2005.10.27 Pawel Cichocki: added some comments
- 2005.10.26 Michal Nowacki: creation - separated from another file

COPYRIGHT:
Copyright (c) 2005 Instytut Informatyki, Politechnika Warszawska
ALL RIGHTS RESERVED
*******************************************************************************/

#include <iostream>

#ifdef _SUNOS
#include "/materialy/AISDI/tree/TreeMap.h"
#include "timer.h"
#else
#include "TreeMap.h"
#endif

int CCount::count=0;

using namespace std;

void test();

int main()
{
    TreeMap t;

    cout << "insert" << endl;
    t.insert(std::make_pair(5, "5"));
    t.insert(std::make_pair(10, "10"));
    t.insert(std::make_pair(1, "1"));
    t.insert(std::make_pair(2, "2"));
    t.insert(std::make_pair(7, "7"));
    t.insert(std::make_pair(6, "6"));
    t.insert(std::make_pair(11, "11"));
    t.insert(std::make_pair(9, "9"));
    t.insert(std::make_pair(13, "13"));
    cout << t.insert(std::make_pair(3, "3")).second << endl;
    cout << t.insert(std::make_pair(9, "3")).second << endl;

    for (TreeMap::iterator i = t.begin(); i != t.end(); i++) {
        cout << i->first << " " << i->second << endl;
    }
    cout << endl;
    for (TreeMap::iterator i = --t.end(); i != t.begin(); i--) {
        cout << i->first << " " << i->second << endl;
    }

    cout << "find" << endl;
    cout << t.find(9)->second << endl;
    cout << t.find(4)->second << endl;

    cout << "empty?" << endl;
    cout << t.empty() << endl;
    TreeMap empty;
    cout << empty.empty() << endl;

    cout << "size" << endl;
    cout << t.size() << endl;
    cout << empty.size() << endl;

    cout << "count" << endl;
    cout << t.count(10) << endl;
    cout << empty.count(3) << endl;

    cout << "erase" << endl;
    cout << t.erase(5) << endl;
    cout << t.erase(20) << endl;

    t.insert(std::make_pair(27, "27"));
    t.insert(std::make_pair(36, "36"));
    t.insert(std::make_pair(17, "17"));
    t.insert(std::make_pair(35, "35"));
    t.insert(std::make_pair(37, "37"));
    t.insert(std::make_pair(39, "39"));
    t.insert(std::make_pair(23, "23"));
    t.insert(std::make_pair(19, "19"));
    t.insert(std::make_pair(32, "32"));
    t.insert(std::make_pair(24, "24"));
    t.insert(std::make_pair(29, "29"));
    t.insert(std::make_pair(25, "25"));

    t.erase(t.find(27), t.find(35));
    cout << t.erase(t.find(6))->first << endl;

    for (TreeMap::iterator i = t.begin(); i != t.end(); i++) {
        cout << i->first << " " << i->second << endl;
    }

    cout << "cmp" << endl;
    TreeMap copy = t;
    cout << copy.struct_eq(t) << endl;
    copy.erase(13);
    copy.insert(std::make_pair(13, "13"));
    cout << copy.struct_eq(t) << endl;
    cout << copy.info_eq(t) << endl;

    return 0;
}
