/**
@file asd.cc

Plik do modyfikacji w w ramach cwiczenia 4 z AISDI.
Zawiera niekompletne implementacje metod klasy TreeMap.
Jest tez prosta funkcja testujaca (void test()), ktora
jest wolana w funkcji main. Mozna w niej zaimplementowac
wlasne testy.
NALEZY ZMODYFIKOWAC I UZUPELNIC CIALA METOD KLASY TreeMap.

@author
Pawel Cichocki, Michal Nowacki

@date
last revision
- 2005.12.01 Pawel Cichocki: TreeNodeDetail class
- 2005.12.01 Michal Nowacki: lab #4
- 2005.11.17 Michal Nowacki: lab #3, copy operator and constructor
- 2005.11.04 Pawel Cichocki: copied comments from the header
- 2005.11.03 Pawel Cichocki: const_iterator done properly now
- 2005.10.27 Pawel Cichocki: cosmetic changes
- 2005.10.26 Michal Nowacki: removed some method bodies
- 2005.10.25 Pawel Cichocki: wrote it

COPYRIGHT:
Copyright (c) 2005 Instytut Informatyki, Politechnika Warszawska
ALL RIGHTS RESERVED
*******************************************************************************/

#include <assert.h>
#include <algorithm>
#include <iostream>
#include <utility>
#include <limits.h>

#ifdef _SUNOS
#include "/materialy/AISDI/tree/TreeMap.h"
#else
#include "TreeMap.h"
#endif

/// A helper class.
class TreeMapDetail //Helper
{
protected:
    friend class TreeMap;
    typedef int Key;
    typedef std::string Val;
    typedef std::pair<int,std::string> T;

    static TreeNode* copy_tree(TreeNode*, TreeNode*);
    static TreeNode* min(TreeNode*);
    static TreeNode* max(TreeNode*);
    static TreeNode* next(TreeNode*);
    static TreeNode* prev(TreeNode*);
    static TreeNode* insert(TreeNode*, const T&);
    static TreeNode* find(TreeNode*, const Key&);
    static void erase(TreeNode*);
    static bool struct_eq(TreeNode*, TreeNode*);
    static int height(TreeNode*);
    static int b(TreeNode*);
    static void balance(TreeNode*);
    static void rotate_left(TreeNode*);
    static void rotate_right(TreeNode*);
    static void rotate_left_twice(TreeNode*);
    static void rotate_right_twice(TreeNode*);
};

TreeNode* TreeMapDetail::copy_tree(TreeNode* parent, TreeNode* src)
{
    if (src) {
        TreeNode* dst = new TreeNode(src->data, parent);
        dst->left = TreeMapDetail::copy_tree(dst, src->left);
        dst->right = TreeMapDetail::copy_tree(dst, src->right);
        return dst;
    }
    return NULL;
}

// Zwraca najmniejszy element (skrajnie lewy).
TreeNode* TreeMapDetail::min(TreeNode* node)
{
    if (node->left) {
        return TreeMapDetail::min(node->left);
    }
    return node;
}

// Zwraca największy element (skrajnie prawy).
TreeNode* TreeMapDetail::max(TreeNode* node)
{
    if (node->right) {
        return TreeMapDetail::max(node->right);
    }
    return node;
}

// Następny element w kolejności po węźle.
TreeNode* TreeMapDetail::next(TreeNode* node)
{
    if (node->right) {          // Istnieje prawe poddrzewo?
        // Wejdź w nie.
        // I przejdź do maksymalnie w lewo.
        return TreeMapDetail::min(node->right);
    }

    // Znajdź pierwszego rodzica, dla którego węzeł leży w lewym poddrzewie.
    while (node->parent && node->parent->left != node) {
        node = node->parent;
    }
    return node->parent;
}

// Poprzedni element w kolejności po węźle.
// Analogicznie do funkcji next.
TreeNode* TreeMapDetail::prev(TreeNode* node)
{
    if (node->left) {
        return TreeMapDetail::max(node->left);
    }

    while (node->parent && node->parent->right != node) {
        node = node->parent;
    }
    return node->parent;
}

TreeNode* TreeMapDetail::insert(TreeNode* node, const T& entry)
{
    TreeNode *result;

    if (entry.first < node->data.first) { // Wstaw w lewym poddrzewie
        if (node->left) {
            result = TreeMapDetail::insert(node->left, entry);
        } else {
            result = node->left = new TreeNode(entry, node);
        }
    } else {                    // Wstaw w prawym podrzewie
        if (node->right) {
            result = TreeMapDetail::insert(node->right, entry);
        } else {
            result = node->right = new TreeNode(entry, node);
        }
    }
    // Po powrocie z rekurencji wyważ każdy węzeł aż do korzenia.
    TreeMapDetail::balance(node);
    return result;
}

TreeNode* TreeMapDetail::find(TreeNode* node, const Key& k)
{
    if (!node) {
        return NULL;
    } else if (node->data.first < k) {
        return TreeMapDetail::find(node->right, k);
    } else if (node->data.first > k) {
        return TreeMapDetail::find(node->left, k);
    }
    return node;
}

void TreeMapDetail::erase(TreeNode* node)
{
    TreeNode* parent_node = node->parent;
    if (!node->left && !node->right) { // Liść
        if (parent_node->left == node) {
            parent_node->left = NULL;
        } else {
            parent_node->right = NULL;
        }
        delete node;
    } else if (!node->left || !node->right) { // Jedno poddrzewo
        TreeNode* child_node = node->left ? node->left : node->right;
        child_node->parent = parent_node;
        if (parent_node->left == node) {
            parent_node->left = child_node;
        } else {
            parent_node->right = child_node;
        }
        delete node;
    } else {                    // Dwa poddrzewa
        // Usuń poprzednik
        TreeNode* previous_node = TreeMapDetail::prev(node);
        node->data = previous_node->data;
        TreeMapDetail::erase(previous_node);
        return;
    }

    // Należy wyważać drzewo aż do korzenia.
    while (parent_node->parent) { // Iteruj do strażnika wyłącznie
        TreeMapDetail::balance(parent_node);
        parent_node = parent_node->parent;
    }
}

bool TreeMapDetail::struct_eq(TreeNode* node, TreeNode* another)
{
    if (!node && !another) {    // Oba wskaźniki są nullami -> poddrzewa takie same
        return true;
    } else if (!node || !another) { // Tylko jeden ze wskaźników doszedł do
                                    // końca poddrzewa -> poddrzewa różne
        return false;
    }

    if (node->data == another->data) { // Pary takie same w obu węzłach?
        // Porównaj rekurencyjnie lewe i prawe poddrzewa węzłów.
        bool left_result = TreeMapDetail::struct_eq(node->left, another->left);
        bool right_result = TreeMapDetail::struct_eq(node->right, another->right);

        // Jeśli poddrzewa są równe to i drzewo jest równe.
        return left_result && right_result;
    }

    return false;
}

// Zwraca wysokość drzewa.
int TreeMapDetail::height(TreeNode* node)
{
    if (node) {
        return 1 + std::max(TreeMapDetail::height(node->left),
                            TreeMapDetail::height(node->right));
    }
    return 0;
}

// Zwraca współczynnik wyważenia drzewa.
int TreeMapDetail::b(TreeNode* node)
{
    return TreeMapDetail::height(node->left) - TreeMapDetail::height(node->right);
}

void TreeMapDetail::balance(TreeNode* node)
{
    if (TreeMapDetail::b(node) == -2) {
        if (TreeMapDetail::b(node->right) <= 0) {
            TreeMapDetail::rotate_left(node);
        } else if (TreeMapDetail::b(node->right) == 1){
            TreeMapDetail::rotate_left_twice(node);
        }
    } else if (TreeMapDetail::b(node) == 2) {
        if (TreeMapDetail::b(node->left) <= 0) {
            TreeMapDetail::rotate_right(node);
        } else if (TreeMapDetail::b(node->left) == -1) {
            TreeMapDetail::rotate_right_twice(node);
        }
    }
}

void TreeMapDetail::rotate_left(TreeNode* node)
{
    TreeNode *n2 = node->right;
    node->right = n2->left;
    if (node->right) {
        node->right->parent = node;
    }
    n2->parent = node->parent;
    // Popraw wskaźnik rodzica na dziecko (węzeł, który rotujemy).
    if (n2->parent->left == node) {
        n2->parent->left = n2;
    } else {
        n2->parent->right = n2;
    }
    node->parent = n2;
    n2->left = node;
}

void TreeMapDetail::rotate_right(TreeNode* node)
{
    TreeNode *n2 = node->left;
    node->left = n2->right;
    if (node->left) {
        node->left->parent = node;
    }
    n2->parent = node->parent;
    if (n2->parent->left == node) {
        n2->parent->left = n2;
    } else {
        n2->parent->right = n2;
    }
    node->parent = n2;
    n2->right = node;
}

void TreeMapDetail::rotate_left_twice(TreeNode* node)
{
    TreeMapDetail::rotate_right(node->right);
    TreeMapDetail::rotate_left(node);
}

void TreeMapDetail::rotate_right_twice(TreeNode* node)
{
    TreeMapDetail::rotate_left(node->left);
    TreeMapDetail::rotate_right(node);
}

//////////////////////////////////////////////////////////////////////////////
// TreeMap and TreeMap::iterator methods
//////////////////////////////////////////////////////////////////////////////

TreeMap::TreeMap()
{
    root = new TreeNode(std::make_pair(INT_MAX, "trash"));
};

/// Content of existing TreeMap object is copied into the new object.
TreeMap::TreeMap( const TreeMap& m )
{
    root = TreeMapDetail::copy_tree(NULL, m.root);
};

TreeMap::~TreeMap()
{
    clear();
    delete root;
};

// Inserts an element into the map.

// @returns A pair whose bool component is true if an insertion was
//          made and false if the map already contained an element
//          associated with that key, and whose iterator component coresponds to
//          the address where a new element was inserted or where the element
//          was already located.
std::pair<TreeMap::iterator, bool> TreeMap::insert(const std::pair<Key, Val>& entry)
{
    iterator it = find(entry.first);
    if (it == end()) {
        return std::make_pair(unsafe_insert(entry), true);
    }
    return std::make_pair(it, false);
}

// Inserts an element into the map.
// This method assumes there is no value asociated with
// such a key in the map.

TreeMap::iterator TreeMap::unsafe_insert(const std::pair<Key, Val>& entry)
{
    if (root->left) {
        return iterator(TreeMapDetail::insert(root->left, entry));
    }
    // Drzewo puste - wstaw ręcznie pierwszy element.
    return iterator(root->left = new TreeNode(entry, root));
}

// Returns an iterator addressing the location of the entry in the map
// that has a key equivalent to the specified one or the location succeeding the
// last element in the map if there is no match for the key.
TreeMap::iterator TreeMap::find(const Key& k)
{
    if (TreeNode* result = TreeMapDetail::find(root->left, k)) {
        return iterator(result);
    }
    return end();
}

TreeMap::const_iterator TreeMap::find(const Key& k) const
{
    if (TreeNode* result = TreeMapDetail::find(root->left, k)) {
        return const_iterator(result);
    }
    return end();
}

// Inserts an element into a map with a specified key value
// if one with such a key value does not exist.
// @returns Reference to the value component of the element defined by the key.
TreeMap::Val& TreeMap::operator[](const Key& k)
{
    return insert(std::make_pair(k, Val())).first->second;
}

// Tests if a map is empty.
bool TreeMap::empty( ) const
{
    return root->left == NULL;
}

// Returns the number of elements in the map.
TreeMap::size_type TreeMap::size( ) const
{
    size_type len = 0;

    for (const_iterator it = begin(); it != end(); ++it) {
        ++len;
    }
    return len;
}

// Returns the number of elements in a map whose key matches a parameter-specified key.
TreeMap::size_type TreeMap::count(const Key& _Key) const
{
    if (TreeMapDetail::find(root->left, _Key)) {
        return 1;
    }
    return 0;
}

// Removes an element from the map.
// @returns The iterator that designates the first element remaining beyond any elements removed.
TreeMap::iterator TreeMap::erase(TreeMap::iterator i)
{
    if (i == end()) {
        return i;
    }

    Node *next = TreeMapDetail::next(i.node);
    TreeMapDetail::erase(i.node);
    return iterator(next);
}

// Removes a range of elements from the map.
// The range is defined by the key values of the first and last iterators
// first is the first element removed and last is the element just beyond the last elemnt removed.
// @returns The iterator that designates the first element remaining beyond any elements removed.
TreeMap::iterator TreeMap::erase(TreeMap::iterator f, TreeMap::iterator l)
{
    while (f != l) {
        f = erase(f);
    }

    return l;
}

// Removes an element from the map.
// @returns The number of elements that have been removed from the map.
//          Since this is not a multimap itshould be 1 or 0.
TreeMap::size_type TreeMap::erase(const Key& key)
{
    if (erase(find(key)) == end()) {
        return 0;
    }
    return 1;
}

// Erases all the elements of a map.
void TreeMap::clear( )
{
    erase(begin(), end());
}

bool TreeMap::struct_eq(const TreeMap& another) const
{
    return TreeMapDetail::struct_eq(root->left, another.root->left);
}

bool TreeMap::info_eq(const TreeMap& another) const
{
    for (const_iterator it = begin(), another_it = another.begin();
         it.node->data == another_it.node->data;
         ++it, ++another_it) {
        if (it == end()) {
            // Doszliśmy do strażnika(ów) - czyli drzewa zawierają te same pary.
            return true;
        }
    }
    return false;
}

// preincrement
TreeMap::const_iterator& TreeMap::const_iterator::operator ++()
{
    node = TreeMapDetail::next(node);
    return *this;
}

// postincrement
TreeMap::const_iterator TreeMap::const_iterator::operator++(int)
{
    const_iterator tmp = *this;
    ++*this;
    return tmp;
}

// predecrement
TreeMap::const_iterator& TreeMap::const_iterator::operator--()
{
    node = TreeMapDetail::prev(node);
    return *this;
}

// postdecrement
TreeMap::const_iterator TreeMap::const_iterator::operator--(int)
{
    const_iterator tmp = *this;
    --*this;
    return tmp;
}


/// Assignment operator copy the source elements into this object.
TreeMap& TreeMap::operator=(const TreeMap& other)
{
    if (this != &other) {
        clear();
        root->left = TreeMapDetail::copy_tree(root, other.root->left);
    }
    return *this;
}

/// Returns an iterator addressing the first element in the map
TreeMap::iterator TreeMap::begin()
{
    return iterator(TreeMapDetail::min(root));
}

TreeMap::const_iterator TreeMap::begin() const
{
    return const_iterator(TreeMapDetail::min(root));
}

/// Returns an iterator that addresses the location succeeding the last element in a map
TreeMap::iterator TreeMap::end()
{
   return iterator(root);
}

/// Returns an iterator that addresses the location succeeding the last element in a map
TreeMap::const_iterator TreeMap::end() const
{
   return const_iterator(root);
}

//////////////////////////////////////////////////////////////////////////////
// Tests
//////////////////////////////////////////////////////////////////////////////

/// A helper function that outputs a key-value pair.
void print(const std::pair<int, std::string>& p)
{
   std::cout<<p.first<<", "<<p.second<<std::endl;
}

#include <map>

/// The big mean test function ;)
void test()
{
   // A typedef used by the test.
   typedef std::map<int, std::string> TEST_MAP;
   //typedef SmallMap<int, std::string> TEST_MAP;
   //typedef TreeMap TEST_MAP;

   std::cout << "Testy uzytkownika" << std::endl;

        TEST_MAP m;

   m[2] = "Merry";
   m[4] = "Jane";
   m[8] = "Korwin";
   m[4] = "Magdalena";

   for_each(m.begin(), m.end(), print );
   //system("PAUSE");

}

//////////////////////////////////////////////////////////////////////////////
// main - jest w pliku /materialy/AISDI/z2/main.cc
//////////////////////////////////////////////////////////////////////////////

//int main()
//{
//   std::cout << "AISDI cwiczenie 4: wchodze do funkcji main." << std::endl;

//   test();
//   // Biblioteka z bardziej rygorystyczna wersja tych testow bedzie udostepniona na nastepnych zajeciach.
//   Test2();
//   //system("PAUSE");
//   return EXIT_SUCCESS;
//}
